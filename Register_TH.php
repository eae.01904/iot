<?php 
    session_start();
    include('Server.php'); 
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>ลงทะเบียน</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
        <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-dark">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4">
                        <div class="row justify-content-center">
                            <div class="col-lg-7">
                                <!-- Basic registration form-->
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header justify-content-center"><h3 class="fw-light my-4">สร้างบัญชี</h3></div>
                                    <div class="card-body">
                                        <!-- Registration form-->
                                        <form action="Register_TH_db.php" method="post">
                                            <?php include('errors.php'); ?>
                                            <!-- notification message -->
                                            <?php if (isset($_SESSION['success'])) : ?>
                                                <div class="success">
                                                    <h3>
                                                        <?php
                                                            echo $_SESSION['success'];
                                                            unset($_SESSION['success']);
                                                        ?>
                                                    </h3>
                                                </div>
                                            <?php endif ?>
                                            <!-- Form Row-->
                                            <div class="row gx-3">
                                                <div class="col-md-12">
                                                    <!-- Form Group (username)-->
                                                    <div class="mb-3">
                                                        <label class="small mb-1" for="username">ชื่อผู้ใช้</label>
                                                        <input class="form-control" name="username" id="username" type="text" placeholder="ใส่ชื่อผู้ใช้" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Group (email address)            -->
                                            <div class="mb-3">
                                                <label class="small mb-1" for="email">อีเมล</label>
                                                <input class="form-control" name="email" id="email" type="email" aria-describedby="emailHelp" placeholder="ใส่ที่อยู่อีเมล" />
                                            </div>
                                            <!-- Form Row    -->
                                            <div class="row gx-3">
                                                <div class="col-md-6">
                                                    <!-- Form Group (password)-->
                                                    <div class="mb-3">
                                                        <label class="small mb-1" for="password_1">รหัสผ่าน</label>
                                                        <input class="form-control" id="password_1" name="password_1" type="password" placeholder="ใส่รหัสผ่าน" />
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <!-- Form Group (confirm password)-->
                                                    <div class="mb-3">
                                                        <label class="small mb-1" for="password_2">ยืนยันรหัสผ่าน</label>
                                                        <input class="form-control" id="password_2" name="password_2" type="password" placeholder="ยืนยันรหัสผ่าน" />
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Form Group (create account submit)-->
                                            <div class="input-group">
                                                <button type="submit" name="reg_user1" class="btn btn-primary btn-block">สร้างบัญชี</button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="Login_TH.php">มีบัญชี? เข้าระบบ</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                        <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">ลิขสิทธิ์ &copy; แพลตฟอร์มอินเตอร์เน็ตของสรรพสิ่งพร้อมกรณีศึกษาการควบคุมอุณหภูมิและความชื้นของฟาร์มจิ้งหรีด</div>
                            <div class="col-md-6 text-md-end small">
                                <a href="Register_EN.php">English</a>
                                    &middot;
                                <a href="Register_TH.php">ภาษาไทย</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
