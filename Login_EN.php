<?php include('Server.php'); ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Login</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link rel="icon" type="image/x-icon" href="assets/img/favicon.png" />
        <script data-search-pseudo-elements defer src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/js/all.min.js" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/feather-icons/4.28.0/feather.min.js" crossorigin="anonymous"></script>
    </head>
    <body class="bg-dark">
        <div id="layoutAuthentication">
            <div id="layoutAuthentication_content">
                <main>
                    <div class="container-xl px-4">
                        <div class="row justify-content-center">
                            <div class="col-lg-5">
                                <!-- Basic login form-->
                                <div class="card shadow-lg border-0 rounded-lg mt-5">
                                    <div class="card-header justify-content-center"><h3 class="fw-light my-4">Login</h3></div>
                                    <div class="card-body">
                                        <!-- Login form-->
                                        <form action="Login_EN_db.php" method="post">
                                            <!-- notification message -->
                                            <?php if (isset($_SESSION['error'])) : ?>
                                                <div class="error">
                                                    <h3>
                                                        <?php
                                                            echo $_SESSION['error'];
                                                            unset($_SESSION['error']);
                                                        ?>
                                                    </h3>
                                                </div>
                                            <?php endif ?>
                                            <!-- Form Group (Username address)-->
                                            <div class="mb-3">
                                                <label class="small mb-1" for="username">Username</label>
                                                <input class="form-control" id="username" name="username" type="text" placeholder="Enter username" />
                                            </div>
                                            <!-- Form Group (password)-->
                                            <div class="mb-3">
                                                <label class="small mb-1" for="password">Password</label>
                                                <input class="form-control" name="password" id="password" type="password" placeholder="Enter password" />
                                            </div>
                                            <!-- Form Group (remember password checkbox)-->
                                            <div class="checkbox mb-3">
                                                <label>
                                                    <input type="checkbox" value="remember-me"> Remember me
                                                </label>
                                            </div>
                                            <!-- Form Group (login box)-->
                                            <div class="d-flex align-items-center justify-content-between mt-2 mb-4">
                                                <div class="input-group">
                                                    <button type="submit" name="login_user" class="btn btn-primary">Login</button>
                                                </div>
                                            <div class="col-md-6 text-md-end">
                                                <a class="small" href="Forgotpassword_EN.php">Forgot Password?</a>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="card-footer text-center">
                                        <div class="small"><a href="Register_EN.php">Need an account? Sign up!</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
            <div id="layoutAuthentication_footer">
                <footer class="footer-admin mt-auto footer-dark">
                        <div class="container-xl px-4">
                        <div class="row">
                            <div class="col-md-6 small">Copyright &copy; แพลตฟอร์มอินเตอร์เน็ตของสรรพสิ่งพร้อมกรณีศึกษาการควบคุมอุณหภูมิและความชื้นของฟาร์มจิ้งหรีด</div>
                            <div class="col-md-6 text-md-end small">
                                <a href="Login_EN.php">English</a>
                                    &middot;
                                <a href="Login_TH.php">ภาษาไทย</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
        <script src="js/scripts.js"></script>
    </body>
</html>
